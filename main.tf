resource "opentelekomcloud_dns_zone_v2" "bum_zone" {
  name        = var.DNS_NAME
  description = "Bundesmessenger auto-generated"
  ttl         = 6000
  type        = "public"
}
resource "opentelekomcloud_dns_recordset_v2" "rs_root" {
  zone_id     = opentelekomcloud_dns_zone_v2.bum_zone.id
  name        = var.DNS_NAME
  description = "root"
  ttl         = 3000
  type        = "A"
  records     = [var.LOAD_BALANCER]
}
resource "opentelekomcloud_dns_recordset_v2" "rs_element" {
  zone_id     = opentelekomcloud_dns_zone_v2.bum_zone.id
  name        = join(".",["element",var.DNS_NAME])
  description = "weblient"
  ttl         = 3000
  type        = "A"
  records     = [var.LOAD_BALANCER]
}
resource "opentelekomcloud_dns_recordset_v2" "rs_matrix_admin" {
  zone_id     = opentelekomcloud_dns_zone_v2.bum_zone.id
  name        = join(".",["matrix-admin",var.DNS_NAME])
  description = "matrix-admin"
  ttl         = 3000
  type        = "A"
  records     = [var.LOAD_BALANCER]
}
resource "opentelekomcloud_dns_recordset_v2" "rs_adminapi" {
  zone_id     = opentelekomcloud_dns_zone_v2.bum_zone.id
  name        = join(".",["adminapi",var.DNS_NAME])
  description = "adminapi"
  ttl         = 3000
  type        = "A"
  records     = [var.LOAD_BALANCER]
}
resource "opentelekomcloud_dns_recordset_v2" "rs_keycloak" {
  zone_id     = opentelekomcloud_dns_zone_v2.bum_zone.id
  name        = join(".",["keycloak",var.DNS_NAME])
  description = "keycloak"
  ttl         = 3000
  type        = "A"
  records     = [var.LOAD_BALANCER]
}
# --------------------------------------------------------------START OF KEYCLOAK DEPLOYMENT---------------------------------------------------------------------------------
resource "kubectl_manifest" "kubernetes_namespace" {
  yaml_body = <<YAML
apiVersion: v1
kind: Namespace
metadata:
  name: bum-${var.CUSTOMER_NAME}
YAML
}
resource "gitlab_project" "customer_project" {
  name        = var.CUSTOMER_NAME
  description = "Bundesmessenger auto generated project"
  visibility_level = "private"
  namespace_id = var.GITLAB_CUSTOMER_NAMESPACE
}
resource "gitlab_repository_file" "customer_values" {
  project        = gitlab_project.customer_project.id
  file_path      = "customer_values.yaml"
  branch         = var.GIT_BRANCH
  content        = base64encode(file("${path.module}/customer_values.yaml"))
  author_email   = "terraform@telekom.de"
  author_name    = "Terraform"
  commit_message = "add test file"
}
resource "random_password" "password_postgres" {
  length           = 32
  special          = true
  override_special = "!()-_="
}
resource "postgresql_role" "database_role" {
  depends_on = [random_password.password_postgres]
  name     = var.CUSTOMER_NAME
  login    = true
  password = random_password.password_postgres.result
}
resource "postgresql_database" "keycloak_database" {
  name              = "keycloak_${var.CUSTOMER_NAME}_db"
  owner             = var.CUSTOMER_NAME
  connection_limit  = -1
  allow_connections = true
  depends_on = [postgresql_role.database_role]
}
resource "kubectl_manifest" "argocd_repo" {
  yaml_body = file("${path.module}/argo_repo.yaml")
}
resource "kubectl_manifest" "argocd_app_bum" {
  depends_on = [kubectl_manifest.argocd_repo, postgresql_database.customer_database, gitlab_repository_file.customer_values]
  yaml_body = file("${path.module}/argo_app.yaml")
}
resource "kubectl_manifest" "argocd_app_keycloak" {
  depends_on = [kubectl_manifest.argocd_repo, postgresql_database.keycloak_database, local_file.modified_secret_keycloak]
  yaml_body = file("${path.module}/argo_app_keycloak.yaml")
}
resource "kubectl_manifest" "keycloak_network_policy" {
  yaml_body = file("${path.module}/keycloak_network_policy.yaml")
}
# --------------------------------------------------------------END OF KEYCLOAK DEPLOYMENT-----------------------------------------------------------------------------------
resource "postgresql_database" "customer_database" {
  name              = "synapse_${var.CUSTOMER_NAME}_db"
  owner             = var.CUSTOMER_NAME
  template          = "template0"
  lc_collate        = "C"
  lc_ctype          = "C"
  connection_limit  = -1
  allow_connections = true
  depends_on = [postgresql_role.database_role]
}
locals {
  file_content_secret = file("${path.module}/secret_values.yaml")
  file_content_secret_keycloak = file("${path.module}/keycloak_values.yaml") 
}
resource "local_file" "modified_secret" {
  content  = replace(local.file_content_secret, "{{PASSWORD_PLACEHOLDER}}", random_password.password_postgres.result)
  filename = "secretvalues.yaml"
}
resource "local_file" "modified_secret_keycloak" {
  content = replace(local.file_content_secret_keycloak, "{{PASSWORD_PLACEHOLDER}}", random_password.password_postgres.result)
  filename = "secretvalueskeycloak.yaml"
}
